package ce.studio.scantuntra;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class ListBerita extends AppCompatActivity {

    TextToSpeech textToSpeech;

    String narasi,datatidakditemukan;

    private final int REQ_CODE_SPEECH_INPUT = 100;

    private static final String TAG_RESULT = "result";
    private static final String TAG_CARIBERITA = "cariberita";
    private static final String TAG_DESKRIPSIBERITA = "deskripsiberita";

    static boolean a=false;

    JSONArray rs = null;

    public static String FEED_URL;

    ProgressDialog pd;

    String namaberita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_berita);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        LinearLayout screentap = (LinearLayout) findViewById(R.id.screentap);

        narasi = "1 kali ketuk untuk membaca berita 1, 2 kali ketuk untuk membaca berita 2, 3 kali ketuk untuk mencari berita";
        datatidakditemukan = "Kata kunci yang anda masukkan tidak ditemukan";

        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                textToSpeech.setLanguage(new Locale("id", "ID"));
                if (status == TextToSpeech.SUCCESS) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        textToSpeech.speak(narasi, TextToSpeech.QUEUE_FLUSH, null, null);

                    } else {
                        textToSpeech.speak(narasi, TextToSpeech.QUEUE_FLUSH, null);
                    }
                }
            }
        });

        screentap.setOnTouchListener(new View.OnTouchListener() {
            Handler handler = new Handler();

            int numberOfTaps = 0;
            long lastTapTimeMs = 0;
            long touchDownMs = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        touchDownMs = System.currentTimeMillis();
                        break;
                    case MotionEvent.ACTION_UP:
                        handler.removeCallbacksAndMessages(null);

                        if ((System.currentTimeMillis() - touchDownMs) > ViewConfiguration.getTapTimeout()) {
                            //it was not a tap

                            numberOfTaps = 0;
                            lastTapTimeMs = 0;
                            break;
                        }

                        if (numberOfTaps > 0
                                && (System.currentTimeMillis() - lastTapTimeMs) < ViewConfiguration.getDoubleTapTimeout()) {
                            numberOfTaps += 1;
                        } else {
                            numberOfTaps = 1;
                        }

                        lastTapTimeMs = System.currentTimeMillis();

                        if (numberOfTaps == 1) {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }, ViewConfiguration.getDoubleTapTimeout());

                        } else if (numberOfTaps == 2) {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }, ViewConfiguration.getDoubleTapTimeout());
                        } else if (numberOfTaps == 3) {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    InputSuara();
                                }
                            }, ViewConfiguration.getDoubleTapTimeout());
                        }
                }

                return true;
            }
        });

    }

    public void InputSuara() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Masukkan kata kunci berita");

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }

    }

    //Untuk menerima inputan speech dan menampilkan text
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    namaberita=result.get(0);

                    new cekberita().execute();
                }
                break;
            }

        }
    }

    @Override
    public void onPause() {
        // Don't forget to shutdown tts!
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    private class cekberita extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(ListBerita.this);
            pd.setMessage("Sedang mengecek berita, tunggu sebentar...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://"+getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","")+"/scantuntra/berita.php";
            FEED_URL += "?berita="+namaberita.replace(" ","%20");

            Log.e("Berita =",FEED_URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strcariberita = a.getString(TAG_CARIBERITA);

                        if (strcariberita.equals("1")){
                            String strdeskripsiberita= a.getString(TAG_DESKRIPSIBERITA);

                            Intent intent =  new Intent(ListBerita.this,Berita.class);
                            intent.putExtra("deskripsiberita",strdeskripsiberita);
                            startActivity(intent);
                            finish();
                            pd.hide();
                        }
                        else if (strcariberita.equals("0")){
                            textToSpeech = new TextToSpeech(ListBerita.this, new TextToSpeech.OnInitListener() {
                                @Override
                                public void onInit(int status) {
                                    textToSpeech.setLanguage(new Locale("id", "ID"));
                                    if (status == TextToSpeech.SUCCESS) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            textToSpeech.speak(datatidakditemukan, TextToSpeech.QUEUE_FLUSH, null, null);

                                        } else {
                                            textToSpeech.speak(datatidakditemukan, TextToSpeech.QUEUE_FLUSH, null);
                                        }
                                        pd.hide();
                                    }
                                }
                            });
                        }
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");
                pd.hide();
            }

        }

    }
}